#!/usr/bin/env bash

curl 'http://127.0.0.1:3000/api/onDisconnect/' -X POST \
    --data-urlencode "cn=${common_name}" \
    --max-time 15
