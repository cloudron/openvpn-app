
'use strict';

export default {
    getSettings,
    setSettings,

    list,
    createKey,
    getKey,
    revokeKey,
    isRunning,
    onClientConnect,
    onClientDisconnect,
    onLearnAddress,

    init,
};

import Archiver from 'archiver';
import assert from 'assert';
import childProcess from 'child_process';
import fs from 'fs';
import {HttpError} from 'connect-lastmile';
import safe from 'safetydance';
import timers from 'timers';
import superagent from 'superagent';
import database from './database.js';

import { APP_DOMAIN, SERVER_NAME, DATA_DIR, DNSMASQ_RUN_DIR, WG_DATA_DIR, WG_PORT, OPENVPN_SETTINGS_FILE_PATH, EASYRSA_DIR, PKI_DIR } from '../constants.js';

// legacy keys had username:devicename and username/devicename. The former has issues with linux commands thinking : is some hostname and
// the latter has issues with ccd (see #26). We use underscore (_) since cloudron does not allow _ in usernames
const CN_REGEXP = /^([A-Za-z0-9.-]+)(?:\/|:|_)([A-Za-z0-9\-_]+)$/; // ?: means non-capture

// Provide either the 4 args, or nothing
async function clientConfFile({ca, cert, key, tlsAuth} = {}) {
    const enableAuth = false;

    return `# Client
client
tls-client
dev tun
# this will connect with whatever proto DNS tells us (https://community.openvpn.net/openvpn/ticket/934)
proto ${process.env.VPN_UDP_PORT ? 'udp' : 'tcp'}
remote ${APP_DOMAIN} ${process.env.VPN_UDP_PORT || process.env.VPN_TCP_PORT}
resolv-retry infinite
# this will fallback from udp6 to udp4 as well
connect-timeout 5
data-ciphers AES-256-CBC:AES-256-GCM
auth SHA256
script-security 2
keepalive 10 120
remote-cert-tls server
${enableAuth ? 'auth-user-pass' : ''}

# Keys
${ca
        ? '<ca>\n' + ca + '\n</ca>'
        : 'ca ca.crt'
}
${cert
        ? '<cert>\n' + cert + '\n</cert>'
        : 'cert cert.crt'
}
${key
        ? '<key>\n' + key + '\n</key>'
        : 'key cert.key'
}
${tlsAuth
        ? 'key-direction 1\n<tls-auth>\n' + tlsAuth + '\n</tls-auth>'
        : 'tls-auth ta.key 1'
}

# Security
nobind
persist-key
persist-tun
verb 3
`;
}

const gOvConnectedClients = {}; // cn: { removeIp, vpnIp }
let gWgConnectedClients = {};
const gOvHostnames = {};

function easyrsa({tag, args, wantedCode = 0}) {
    assert.strictEqual(typeof tag, 'string');
    assert(Array.isArray(args));

    console.log(`${tag} easyrsa: ${args.join(' ')}`);

    return new Promise((resolve, reject) => {
        const env = Object.assign({}, process.env);
        env.EASYRSA = EASYRSA_DIR;
        env.EASYRSA_PKI = PKI_DIR;
        const cp = childProcess.spawn(`${EASYRSA_DIR}/easyrsa`, args, { env });
        cp.stdout.on('data', data => console.log(`${tag} (stdout): ${data.toString('utf8')}`));

        let errorData = '';
        cp.stderr.on('data', data => {
            errorData += data.toString('utf8'); // print only on newline
            while (errorData.includes('\n')) {
                const parts = errorData.split('\n');
                console.log(`${tag} (stderr): ${parts.shift()}`);
                errorData = parts.join('\n');
            }
        });

        cp.on('exit', (code, signal) => {
            if (errorData) console.log(`${tag} (stderr): ${errorData}`);
            if (code || signal) console.log(`${tag} code: ${code}, signal: ${signal}`);
            if (code === wantedCode) return resolve();

            const e = new Error(`${tag} exited with error ${code} signal ${signal}`);
            e.code = code;
            e.signal = signal;
            reject(e);
        });

        cp.on('error', error => {
            console.log(`${tag} code: ${error.code}, signal: ${error.signal}`);
            reject(error);
        });
    });
}

function cleanDeviceName(deviceName) {
    return deviceName.replace(/[^A-Za-z0-9\-_]+/g, '');
}

async function getSettings() {
    const db = await database.read();
    return {
        ovNetwork: db.ovNetwork,
        ovNetwork6: db.ovNetwork6,
        wgNetwork: db.wgNetwork,
        wgNetwork6: db.wgNetwork6,
        dnsServer: db.dnsServer,
        allowICC: db.allowICC,
        enableAuth: false // db.enableAuth // not implemented
    };
}

async function setSettings(settings) {
    const db = await database.read();
    // ui does not send ipv6 settings yet
    db.ovNetwork = settings.ovNetwork;
    db.wgNetwork = settings.wgNetwork;
    db.dnsServer = settings.dnsServer;
    db.allowICC = settings.allowICC;
    db.enableAuth = false; // settings.enableAuth;

    await database.write(db);

    await ovSyncSettings(db);
    await wgSyncInterfaceConf(db);
}

async function ovSyncSettings(db) {
    let tmp = await fs.promises.readFile(OPENVPN_SETTINGS_FILE_PATH, 'utf-8');
    tmp = tmp.split('\n');

    // allowICC
    let idx = tmp.findIndex((l) => { return l.indexOf('client-to-client') !== -1; });
    let value = (db.allowICC ? '' : '# ') + 'client-to-client';
    if (idx === -1) tmp.push(value);
    else tmp[idx] = value;

    // enableAuth . we can also use --auth-user-pass-optional for optional auth if needed
    idx = tmp.findIndex((l) => { return l.indexOf('auth-user-pass-verify') !== -1; });
    value = (db.enableAuth ? '' : '# ') + `auth-user-pass-verify ${import.meta.dirname}/openvpn-hooks/user-pass-verify.sh via-env`;
    if (idx === -1) tmp.push(value);
    else tmp[idx] = value;

    // address and netmask (only ipv4 for the moment)
    idx = tmp.findIndex((l) => { return l.indexOf('server ') === 0 || l.indexOf('# server ') === 0; });
    value = `server ${db.ovNetwork} 255.255.255.0`;
    if (idx === -1) tmp.push(value);
    else tmp[idx] = value;

    // dnsServer
    idx = tmp.findIndex((l) => { return l.indexOf('push "dhcp-option DNS') !== -1; });
    const dnsServer = db.dnsServer || db.ovNetwork.replace(/\.0$/, '.1');
    value = `push "dhcp-option DNS ${dnsServer}"`;
    if (idx === -1) tmp.push(value);
    else tmp[idx] = value;

    await fs.promises.writeFile(OPENVPN_SETTINGS_FILE_PATH, tmp.join('\n'), 'utf-8');

    if (process.env.VPN_TCP_PORT || process.env.VPN_UDP_PORT) {
        safe.child_process.execSync(`sudo ${import.meta.dirname}/restart.sh`, { stdio: 'inherit' });
        if (safe.error) throw new Error(`Could not restart OpenVPN: ${safe.error.message}`);
    }
}

// client name = username_devicename
function parseClientName(cn) {
    const username = cn.split('_')[0];
    const deviceName = cn.slice(`${username}_`.length);
    return { username, deviceName };
}

async function listClients(db, username) {
    const results = [];

    for (const clientName in db.clients) {
        const parsedCn = parseClientName(clientName);
        if (username !== parsedCn.username) continue;

        results.push(db.clients[clientName]);
    }

    return results;
}

async function list(req, res, next) {
    const [error, db] = await safe(database.read());
    if (error) return next(new HttpError(500, error));

    const username = req.oidc.user.sub;
    const list = await listClients(db, username);

    const devices = [];

    for (const item of list) {
        const cn = `${username}_${item.deviceName}`;

        devices.push({
            name: item.deviceName,
            deviceName: item.deviceName,
            hostname: `${item.deviceName}.${username}`,
            created: item.birthtimeMs,
            connected: gWgConnectedClients[cn] || gOvConnectedClients[cn] || null
        });
    }

    res.status(200).send({ devices });
}

async function wgSaveInterfaceConf(db) {
    const WG_PORT = process.env.WIREGUARD_UDP_PORT || 51820;

    const intf = 'wg0';
    const interfaceAddress = db.wgNetwork.replace(/\.0$/, `.1/24`);
    const interfaceAddress6 = db.wgNetwork6.replace(/::0$/, `::1/64`);

    let intfConf =
`# Server
[Interface]
PrivateKey = ${db.wgPrivateKey}
Address = ${interfaceAddress}, ${interfaceAddress6}
ListenPort = ${WG_PORT}
PreUp =
PostUp = iptables -t nat -A POSTROUTING -s ${db.wgNetwork}/24 -o eth0 -j MASQUERADE
PostUp = iptables -A INPUT -p udp -m udp --dport ${WG_PORT} -j ACCEPT
PostUp = iptables -A FORWARD -i ${intf} -j ACCEPT
PostUp = iptables -A FORWARD -o ${intf} -j ACCEPT
PostUp = ip6tables -t nat -A POSTROUTING -s ${db.wgNetwork6}/64 -o eth0 -j MASQUERADE
PostUp = ip6tables -A INPUT -p udp -m udp --dport ${WG_PORT} -j ACCEPT
PostUp = ip6tables -A FORWARD -i ${intf} -j ACCEPT
PostUp = ip6tables -A FORWARD -o ${intf} -j ACCEPT
PreDown =
PostDown = iptables -t nat -D POSTROUTING -s ${db.wgNetwork}/24 -o eth0 -j MASQUERADE
PostDown = iptables -D INPUT -p udp -m udp --dport ${WG_PORT} -j ACCEPT
PostDown = iptables -D FORWARD -i ${intf} -j ACCEPT
PostDown = iptables -D FORWARD -o ${intf} -j ACCEPT
PostDown = ip6tables -t nat -D POSTROUTING -s ${db.wgNetwork6}/64 -o eth0 -j MASQUERADE
PostDown = ip6tables -D INPUT -p udp -m udp --dport ${WG_PORT} -j ACCEPT
PostDown = ip6tables -D FORWARD -i ${intf} -j ACCEPT
PostDown = ip6tables -D FORWARD -o ${intf} -j ACCEPT
    `;

    for (const clientName in db.clients) {
        const client = db.clients[clientName];

        const peerAddress = db.wgNetwork.replace(/\.0$/, `.${client.wgAddress}/32`);
        const peerAddress6 = db.wgNetwork6.replace(/::0$/, `::${client.wgAddress}/128`);

        intfConf += `
# Client (${clientName})
[Peer]
PublicKey = ${client.wgPublicKey}
PresharedKey = ${client.wgPresharedKey}
AllowedIPs = ${peerAddress}, ${peerAddress6}

`;
    }

    await fs.promises.writeFile(`${WG_DATA_DIR}/${intf}.conf`, intfConf, { encoding: 'utf8', mode: 0o660 });
}

async function wgSyncInterfaceConf(db) {
    await wgSaveInterfaceConf(db);

    safe.child_process.execSync(`sudo wg-quick down ${WG_DATA_DIR}/wg0.conf`, { encoding: 'utf8', stdio: 'inherit' });
    childProcess.execSync(`sudo wg-quick up ${WG_DATA_DIR}/wg0.conf`, { encoding: 'utf8', stdio: 'inherit' });
    const strippedConf = childProcess.execSync(`sudo wg-quick strip ${WG_DATA_DIR}/wg0.conf`, { encoding: 'utf8' });
    await fs.promises.writeFile(`${WG_DATA_DIR}/wg0-stripped.conf`, strippedConf);
    childProcess.execSync(`sudo wg syncconf wg0 ${WG_DATA_DIR}/wg0-stripped.conf`, { encoding: 'utf8', stdio: 'inherit' });
}

async function wgCreatePeer() {
    const privateKey = childProcess.execSync('wg genkey', { encoding: 'utf8' }).trim();
    const publicKey = childProcess.execSync('wg pubkey', { encoding: 'utf8', input: privateKey }).trim();
    const presharedKey = childProcess.execSync('wg genpsk', { encoding: 'utf8' }).trim();

    return { privateKey, publicKey, presharedKey };
}

async function wgAllocateAddress(db) {
    const usedAddresses = Object.keys(db.clients).map(cn => db.clients[cn].wgAddress);
    for (let i = 2; i < 255; i++) {
        if (!usedAddresses.includes(i)) return i;
    }

    throw new Error('Out of address space');
}

async function createKey(req, res, next) {
    const deviceName = decodeURIComponent(req.params[0]);

    if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'));

    const username = req.oidc.user.sub;

    const [error, db] = await safe(database.read());
    if (error) return next(new HttpError(500, error));

    const list = await listClients(db, username);

    if (list.map(e => e.deviceName).includes(deviceName)) return next(new HttpError(409, 'Device already exists'));

    const cn = `${username}_${deviceName}`;

    // OpenVPN
    const [easyrsaError] = await safe(easyrsa({
        tag: 'createKey',
        args: [ 'build-client-full', cn, 'nopass' ]
    }));
    if (easyrsaError) return next(new HttpError(500, easyrsaError));

    // Wireguard
    const { publicKey, privateKey, presharedKey } = await wgCreatePeer();
    const [addressError, freeAddress] = await safe(wgAllocateAddress(db));
    if (error) return next(new HttpError(422, addressError.message));

    db.clients[cn] = {
        username,
        deviceName,
        wgPrivateKey: privateKey,
        wgPublicKey: publicKey,
        wgPresharedKey: presharedKey,
        wgAddress: freeAddress,
        birthtimeMs: Date.now()
    };

    await database.write(db);
    await wgSyncInterfaceConf(db);

    res.status(201).send({created: deviceName});
}

async function getKey(req, res, next) {
    const deviceName = decodeURIComponent(req.params[0]);
    const format = req.query['format'] || 'conf';
    const zip = !(req.query['zip'] === 'false'); // default to true

    if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'));

    const username = req.oidc.user.sub;

    const [error, db] = await safe(database.read());
    if (error) return next(new HttpError(500, error));

    const list = await listClients(db, username);
    if (!list.map(e => e.deviceName).includes(deviceName)) return next(new HttpError(404, 'Not Found'));

    const cn = `${username}_${deviceName}`;

    let internalPathPrefix; // path inside the zip file, if zipped
    let configExt; // extension of the config file

    if (format === 'conf') {
        internalPathPrefix = '';
        configExt = 'conf';
    } else if (format === 'ovpn') {
        internalPathPrefix = '';
        configExt = 'ovpn';
    } else if (format === 'tblk') {
        if (!zip) return next(new HttpError(409, 'Invalid format: cannot disable zip for tblk'));
        internalPathPrefix = `${APP_DOMAIN}-${deviceName}.tblk/Contents/Resources/`;
        configExt = 'ovpn';
    } else if (format === 'wg') {
        configExt = 'conf'; // gnome is sensitive to extension

        const client = db.clients[cn];
        const clientAddress = db.wgNetwork.replace(/\.0$/, `.${client.wgAddress}/32`);
        const dnsServer = db.dnsServer || db.wgNetwork.replace(/\.0$/, '.1');
        const clientAddress6 = db.wgNetwork6.replace(/::0$/, `::${client.wgAddress}/128`);

        res.header('Content-Type', 'text/plain');
        res.header('Content-Disposition', `attachment; filename="${deviceName}.${configExt}"`);

        const data =
`[Interface]
PrivateKey = ${client.wgPrivateKey}
Address = ${clientAddress}, ${clientAddress6}
DNS = ${dnsServer}

[Peer]
PublicKey = ${db.wgPublicKey}
PresharedKey = ${client.wgPresharedKey}
AllowedIPs = 0.0.0.0/0, ::/0
PersistentKeepalive = 0
Endpoint = ${APP_DOMAIN}:${WG_PORT}
`;
        res.send(data);
        return;
    } else {
        return next(new HttpError(409, 'Invalid format'));
    }

    const certFile = `${cn}.crt`;
    const keyFile = `${cn}.key`;

    if (zip) {
        res.header('Content-Type', 'application/zip');
        res.header('Content-Disposition', `attachment; filename="${APP_DOMAIN}-${deviceName}-${format}.zip"`);

        const archive = new Archiver('zip');
        archive.on('warning', (err) => console.error('ZIP WARNING:', err));
        archive.on('error', (err) => {
            console.error('ZIP ERROR:', err);
            res.end();
        });
        archive.pipe(res);

        archive.file(`${PKI_DIR}/ca.crt`, {name: internalPathPrefix + 'ca.crt'});
        archive.file(`${PKI_DIR}/issued/${certFile}`, {name: internalPathPrefix + 'cert.crt'});
        archive.file(`${PKI_DIR}/private/${keyFile}`, {name: internalPathPrefix + 'cert.key'});
        archive.file(`${PKI_DIR}/ta.key`, {name: internalPathPrefix + 'ta.key'});
        archive.append(
            await clientConfFile(),
            {name: internalPathPrefix + 'config.' + configExt}
        );
        archive.finalize();
        return;
    }

    try {
        const ca = await fs.promises.readFile(`${PKI_DIR}/ca.crt`, 'utf8');
        const cert = await fs.promises.readFile(`${PKI_DIR}/issued/${certFile}`, 'utf8');
        const key = await fs.promises.readFile(`${PKI_DIR}/private/${keyFile}`, 'utf8');
        const tlsAuth = await fs.promises.readFile(`${PKI_DIR}/ta.key`, 'utf8');

        res.header('Content-Type', 'application/data');
        res.header('Content-Disposition', `attachment; filename="${APP_DOMAIN}-${deviceName}.${configExt}"`);
        res.send(await clientConfFile({ ca, cert, key, tlsAuth }));
    } catch (error) {
        return next(new HttpError(500, error));
    }
}

async function revokeKey(req, res, next) {
    const deviceName = decodeURIComponent(req.params[0]);

    if (!deviceName || (deviceName !== cleanDeviceName(deviceName))) return next(new HttpError(409, 'Invalid device name'));

    const username = req.oidc.user.sub;

    const [error, db] = await safe(database.read());
    if (error) return next(new HttpError(500, error));

    const list = await listClients(db, username);

    if (!list.map(e => e.deviceName).includes(deviceName)) return next(new HttpError(404, 'Not Found'));

    const cn = `${username}_${deviceName}`; // the script looks for keydir/certNameBase.crt

    try {
        await easyrsa({ tag: 'revokeKey', args: [ 'revoke', cn ], wantedCode: 0 });
        await easyrsa({ tag: 'revokeKey-genCrl', args: [ 'gen-crl' ], wantedCode: 0 });

        delete db.clients[cn];
        await wgSyncInterfaceConf(db);

        await database.write(db);

        res.status(200).send({revoked: deviceName});
    } catch (error) {
        if (error) return next(new HttpError(500, error));
    }
}

async function onClientConnect(req, res, next) {
    const cn = req.body['cn'];
    const remoteIp = req.body['ip'] || req.body['ip6'];
    const vpnIp = req.body['vpnIp'] || req.body['vpnIp6'];

    if (!cn || !remoteIp || !vpnIp) return next(new HttpError(400, 'Invalid Request'));

    const match = CN_REGEXP.exec(cn); // cn is the full path of the key that matched
    if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`));
    const [, user, deviceName] = match;

    gOvConnectedClients[cn] = { type:'openvpn', remoteIp, vpnIp, bytesSent: 0, bytesReceived: 0 };
    res.status(200).send({connected: {user, deviceName, remoteIp, vpnIp}});
}

function onClientDisconnect(req, res, next) {
    const cn = req.body['cn'];

    if (!cn) return next(new HttpError(400, 'Invalid Request'));

    const match = CN_REGEXP.exec(cn); // cn is the full path of the key that matched
    if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`));
    const [, user, deviceName] = match;

    delete gOvConnectedClients[cn];

    return res.status(200).send({disconnected: {user, deviceName}});
}

async function onLearnAddress(req, res, next) {
    const operation = req.body['operation'];
    const vpnIp = req.body['vpnIp'];
    const cn = req.body['cn']; // won't be set for delete

    if (!operation || !vpnIp) return next(new HttpError(400, 'Invalid Request'));
    if (!operation.match(/^(add|update|delete)$/)) return next(new HttpError(400, 'Invalid operation'));
    if (operation.match(/^(add|update)$/) && !cn) return next(new HttpError(400, 'cn is required'));

    if (operation === 'add' || operation === 'update') {
        const match = CN_REGEXP.exec(cn); // cn is the full path of the key that matched
        if (!match) return next(new HttpError(400, `Invalid CN: ${cn}`));
        const [, user, deviceName] = match;

        const hostname = `${deviceName}.${user}`;

        gOvHostnames[vpnIp] = `${hostname} ${hostname}.${APP_DOMAIN}`;
    } else if (operation === 'delete') {
        delete gOvHostnames[vpnIp];
    }

    const hostsFile = Object.keys(gOvHostnames).map((ip) => `${ip} ${gOvHostnames[ip]}`).join('\n') + '\n';
    const [error] = await safe(fs.promises.writeFile(`${DNSMASQ_RUN_DIR}/openvpn_hosts`, hostsFile, 'utf8'));
    if (error) return next(new HttpError(500, error));
    return res.status(200).send({learned: {operation, vpnIp}});
}

async function isRunning() {
    const out = safe.child_process.execSync('supervisorctl status admin',  { encoding: 'utf8' });
    if (!out) return false;
    return out.includes('RUNNING');
}

async function pollOpenVpnStatus() {
    console.log('==> Polling openvpn status');

    const data = safe.fs.readFileSync('/run/openvpn-status.log', 'utf8');
    if (!data) {
        console.log('Could not open openvpn-status.log for parsing', safe.error);
        return;
    }

    const lines = data.split('\n');
    // OpenVPN CLIENT LIST
    // Updated,2025-01-07 10:42:51
    // Common Name,Real Address,Bytes Received,Bytes Sent,Connected Since
    // ...
    // ROUTING TABLE
    for (let i = 3; i < lines.length; i++) {
        const line = lines[i];
        if (line === 'ROUTING TABLE') break;
        // eslint-disable-next-line no-unused-vars
        const [cn, realAddress, bytesReceived, bytesSent, connectedSince ] = line.split(',');
        if (!gOvConnectedClients[cn]) continue;
        gOvConnectedClients[cn].bytesReceived = parseInt(bytesReceived, 10);
        gOvConnectedClients[cn].bytesSent = parseInt(bytesSent, 10);
    }
}

async function pollWgStatus() {
    console.log('==> Polling wireguard status');

    const dump = childProcess.execSync(`sudo wg show wg0 dump`, { encoding: 'utf8' });
    const lines = dump.trim().split('\n');
    const peerLines = lines.slice(1); // first line is about the server itself

    const db = await database.read();
    const clients = db.clients;
    const now = Date.now();

    gWgConnectedClients = {};

    for (const line of peerLines) {
        // eslint-disable-next-line no-unused-vars
        const [ publicKey, preSharedKey, endpoint, allowedIps, latestHandshakeAt, transferRx, transferTx, persistentKeepalive ] = line.split('\t');

        const cn = Object.keys(clients).find(cn => clients[cn].wgPublicKey === publicKey);
        if (!cn) continue;

        if ((now - latestHandshakeAt*1000) > (2*60*1000)) continue; // looks like it went away

        gWgConnectedClients[cn] = {
            type: 'wireguard',
            remoteIp: endpoint.replace(`:${WG_PORT}`, ''), // public endpoint of the other side
            vpnIp: allowedIps.replace('/32', ''),
            bytesReceived: transferRx,
            bytesSent: transferTx,
            latestHandshakeAt // epoch seconds
        };
    }

    const hostnames = [];
    for (const cn of Object.keys(gWgConnectedClients)) {
        const [username, deviceName] = cn.split('_');
        hostnames.push(`${clients[cn].wgAddress} ${deviceName}.${username}.${APP_DOMAIN}`);
    }

    console.log(`==> wireguard ${hostnames.length} / ${Object.keys(gWgConnectedClients).length} connected`);
    await fs.promises.writeFile(`${DNSMASQ_RUN_DIR}/wireguard_hosts`, hostnames.join('\n') + '\n', 'utf8');
}

async function createPKI() {
    if (!fs.existsSync(PKI_DIR)) {
        console.log('==> Init OpenVPN PKI');
        await easyrsa({ tag: 'init-pki', args: [ 'init-pki' ], wantedCode: 0 });

        console.log('==> Creating CA and Server certs');
        await easyrsa({ tag: 'build-ca', args: [ 'build-ca', 'nopass' ], wantedCode: 0 }); // ca.key and ca.crt, index.txt and serial
        await easyrsa({ tag: 'build-server-full', args: [ 'build-server-full', SERVER_NAME, 'nopass' ], wantedCode: 0 }); // cloudron.crt (server certificate)

        console.log('==> Generating static key');
        childProcess.execSync(`openvpn --genkey --secret ${PKI_DIR}/ta.key`,  { encoding: 'utf8', stdio: 'inherit' });
        console.log('==> Generating dh.pem (this takes a while)');
        await easyrsa({ tag: 'gen-dh', args: [ 'gen-dh' ], wantedCode: 0 });
    }

    // initializing / regenerating CRL file . See also (https://forums.openvpn.net/viewtopic.php?t=27090). the CRL has to be republished every year
    console.log('==> Generating CRL');
    await easyrsa({ tag: 'gen-crl', args: [ 'gen-crl' ], wantedCode: 0 });
}

async function generateOpenVPNConfig() {
    let config;

    if (fs.existsSync(`${DATA_DIR}/openvpn.conf`)) {
        config = await fs.promises.readFile(`${DATA_DIR}/openvpn.conf`, 'utf8');
    } else {
        config = await fs.promises.readFile(`${import.meta.dirname}/openvpn.conf.template`, 'utf8');
    }

    if (process.env.VPN_UDP_PORT) {
        config = config.replace(/^proto .*$/m, 'proto udp');
        config = config.replace(/^port .*$/m, `port ${process.env.VPN_UDP_PORT}`);
    } else {
        config = config.replace(/^proto .*$/m, 'proto tcp');
        config = config.replace(/^port .*$/m, `port ${process.env.VPN_TCP_PORT || 7494}`); // the default is only to keep the vpn config valid when both tcp & udp are disabled
    }

    // update path to hooks if the source path changes
    config = config.replace(/^client-connect .*$/m, `client-connect ${import.meta.dirname}/openvpn-hooks/on-client-connect.sh`);
    config = config.replace(/^client-disconnect .*$/m, `client-disconnect ${import.meta.dirname}/openvpn-hooks/on-client-disconnect.sh`);
    config = config.replace(/^learn-address .*$/m, `learn-address ${import.meta.dirname}/openvpn-hooks/on-learn-address.sh`);

    // migrate user
    config = config.replace(/^user .*$/m, 'user vpn');
    config = config.replace(/^group .*$/m, 'group vpn');

    config = config.replace(/^push "dhcp-option DOMAIN .*"$/m, `push "dhcp-option DOMAIN ${APP_DOMAIN}"`);
    await fs.promises.writeFile(`${DATA_DIR}/openvpn.conf`, config);

    const [error] = await safe(superagent.get('https://ipv6.api.cloudron.io/api/v1/helper/public_ip').timeout(5000).retry(0));
    if (error) {
        console.log('==> Server has no IPv6 connectivity, disabling default route');
        config = config.replace(/^push "redirect-gateway def1 ipv6 bypass-dhcp"$/m, '# push "redirect-gateway def1 ipv6 bypass-dhcp"');
    } else {
        console.log('==> Server has IPv6 connectivity, setting as default route');
        config = config.replace(/^# push "redirect-gateway def1 ipv6 bypass-dhcp"$/m, 'push "redirect-gateway def1 ipv6 bypass-dhcp"');
    }
}

async function ovInit() {
    const db = await database.read();
    await createPKI();
    await generateOpenVPNConfig();
    await ovSyncSettings(db);
    if (process.env.VPN_TCP_PORT || process.env.VPN_UDP_PORT) {
        timers.setInterval(pollOpenVpnStatus, 20 * 1000); // don't start poller if openvpn not enabled
        console.log('==> OpenVPN Enabled');
    } else {
        console.log('==> OpenVPN Disabled');
    }
}

async function wgInit() {
    const db = await database.read();
    if (!db.wgPrivateKey) {
        db.wgPrivateKey = childProcess.execSync('wg genkey', { encoding: 'utf8' }).trim();
        db.wgPublicKey = childProcess.execSync('wg pubkey', { encoding: 'utf8', input: db.wgPrivateKey }).trim();
        db.wgNetwork = '10.9.0.0';
        db.wgNetwork6 = 'fd42:feed:feed:d0d0::0';
        await database.write(db);
    }

    await fs.promises.mkdir(WG_DATA_DIR, { recursive: true });
    await wgSyncInterfaceConf(db);
    if (process.env.WIREGUARD_UDP_PORT) {
        timers.setInterval(pollWgStatus, 20 * 1000); // don't start poller if wg not enabled
        console.log('==> WireGuard Enabled');
    } else {
        console.log('==> WireGuard Disabled');
    }
}

async function init() {
    const db = await database.read();

    if (!db.ovNetwork) { // first run
        console.log('==> Populating database with default settings');

        db.ovNetwork = '10.8.0.0';
        db.ovNetwork6 = 'fd42:feed:feed:feed::0';
        db.wgNetwork = '10.9.0.0';
        db.wgNetwork6 = 'fd42:feed:feed:d0d0::0';
        db.dnsServer = '';
        db.allowICC = true;
        db.enableAuth = false; // not implemented

        await database.write(db);
    }

    await ovInit();
    await wgInit();
}
