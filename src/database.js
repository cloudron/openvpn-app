'use strict';

import fs from 'fs';
import { VPN_DB } from '../constants.js';

export default {
    read,
    write
};


// should lock at some point
async function read() {
    if (!fs.existsSync(VPN_DB)) return { clients: {} };

    const data = await fs.promises.readFile(VPN_DB, 'utf8');
    const result = JSON.parse(data);
    if (!result.clients) result.clients = {};
    return result;
}

// should lock at some point
async function write(db) {
    await fs.promises.writeFile(VPN_DB, JSON.stringify(db, null, 4), 'utf8');
}
