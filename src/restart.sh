#!/bin/bash

set -eu

echo "==> Restart OpenVPN after config changes"

# Add iptables rules for NATing VPN traffic
network4=$(cat /app/data/openvpn.conf | sed -ne 's/^server \(.*\) .*$/\1/p')
network6=$(cat /app/data/openvpn.conf | sed -ne 's/^server-ipv6 \(.*\)/\1/p')
if nft list table ip nat >/dev/null 2>&1; then
    echo "==> Configuring nft nat rules for ${network4} and ${network6}"
    iptables-nft -t nat -A POSTROUTING -s ${network4}/24 -o eth0 -j MASQUERADE
    ip6tables-nft -t nat -A POSTROUTING -s $network6 -o eth0 -j MASQUERADE
else
    echo "==> Configuring iptables nat rules for ${network4} and ${network6}"
    iptables -t nat -A POSTROUTING -s ${network4}/24 -o eth0 -j MASQUERADE
    ip6tables -t nat -A POSTROUTING -s $network6 -o eth0 -j MASQUERADE
fi

echo "==> Restarting OpenVPN"
supervisorctl restart openvpn

touch /run/openvpn-status.log
chown vpn:vpn /run/openvpn-status.log
