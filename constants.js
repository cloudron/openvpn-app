'use strict';

import path from 'path';

const DATA_DIR = process.env.DATA_DIR || `${import.meta.dirname}/.dev/data`;
const RUN_DIR = process.env.RUN_DIR || `${import.meta.dirname}/.dev/run/`;
const APP_ORIGIN = process.env.APP_ORIGIN || `http://localhost:3000`;
const APP_DOMAIN = process.env.APP_DOMAIN || `localhost`;
const SERVER_NAME = process.env.SERVER_NAME || 'pankow';

// these are derived from above
const EASYRSA_DIR = `${import.meta.dirname}/easyrsa`;
const VPN_DB = `${DATA_DIR}/vpn.db`;
const DNSMASQ_RUN_DIR = `${RUN_DIR}/dnsmasq`;

const WG_DATA_DIR = path.join(DATA_DIR, 'wg');
const WG_PORT = process.env.WIREGUARD_UDP_PORT || 51820;

const OPENVPN_SETTINGS_FILE_PATH = path.join(DATA_DIR, 'openvpn.conf');
const PKI_DIR = `${DATA_DIR}/pki`;

export {
    DATA_DIR,
    RUN_DIR,
    APP_ORIGIN,
    SERVER_NAME,
    APP_DOMAIN,

    // these are derived from above
    EASYRSA_DIR,
    WG_DATA_DIR,
    VPN_DB,
    WG_PORT,
    DNSMASQ_RUN_DIR,
    PKI_DIR,

    OPENVPN_SETTINGS_FILE_PATH
};
