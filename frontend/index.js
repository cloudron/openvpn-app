import { createApp } from 'vue';

import 'primeicons/primeicons.css';

import Index from './Index.vue';

const app = createApp(Index);

app.mount('#app');