FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN adduser --uid 1200 --disabled-login --gecos 'VPN' vpn && passwd -d vpn

# renovate: datasource=repology depName=ubuntu_22_04/openvpn versioning=deb
ARG OPENVPN_VERSION=2.5.11-0ubuntu0.22.04.1
RUN apt-get update -y && \
    apt-get install -y openssl openvpn=${OPENVPN_VERSION} easy-rsa iptables dnsmasq nftables wireguard-tools && \
    rm -rf /var/cache/apt /var/lib/apt/lists

ARG NODE_VERSION=22.11.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN make-cadir /app/code/easyrsa
RUN sed -e '/^RANDFILE.*/d' -i /app/code/easyrsa/openssl-easyrsa.cnf
COPY easyrsa-vars /app/code/easyrsa/vars
# Workaround quantum permissions bug
RUN chown -R vpn:vpn /app/code/easyrsa

## Sudoers
COPY vpn.sudo /etc/sudoers.d/vpn
RUN chmod 0600 /etc/sudoers.d/vpn

## Setting up TUN device
RUN mknod /app/code/net-tun c 10 200

## Supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
# this gives vpn user permissions to control supervisor
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=vpn:vpn,' -i /etc/supervisor/supervisord.conf

## Installing web-admin interface & packaging scripts
COPY package.json package-lock.json vite.config.js index.html /app/code/
COPY public /app/code/public
COPY frontend /app/code/frontend
RUN npm install && \
    npm run build && \
    rm -rf /app/code/frontend /app/code/public /app/code/node_modules && \
    npm install --production

COPY src /app/code/src
COPY start.sh constants.js server.js /app/code/

CMD [ "/app/code/start.sh" ]
