import js from '@eslint/js';
import pluginVue from 'eslint-plugin-vue'
import globals from 'globals';

export default [
  // add more generic rulesets here, such as:
  js.configs.recommended,
  {
      files: ["**/*.js"],
      languageOptions: {
          globals: {
              ...globals.node,
          },
          ecmaVersion: 13,
          sourceType: "commonjs"
      },
      rules: {
          semi: "error",
          "prefer-const": "error"
      }
  },
  ...pluginVue.configs['flat/recommended'],
  // ...pluginVue.configs['flat/vue2-recommended'], // Use this if you are using Vue.js 2.x.
  {
    rules: {
      'no-use-before-define': ['error', 'nofunc'],
      // override/add rules settings here, such as:
      'vue/html-self-closing': 'off',
      'vue/html-closing-bracket-spacing': 'off',
      'vue/singleline-html-element-content-newline': 'off',
      'vue/attributes-order': 'off',
      'vue/max-attributes-per-line': 'off',
      'vue/multi-word-component-names': 'off',
      'vue/no-reserved-component-names': 'off',
    }
  }
]
