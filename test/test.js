#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */
'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    net = require('net'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const VPN_TCP_PORT = 7333, WIREGUARD_UDP_PORT = 51820;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const DEVICE_NAME = 'test_DeV1ce-3';
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement (elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0];
        expect(app).to.be.an('object');
    }

    async function login(alreadyAuthenticated = true) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.id('loginButton'));
        await browser.findElement(By.id('loginButton')).click();

        // some redirects later
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(USERNAME);
            await browser.findElement(By.id('inputPassword')).sendKeys(PASSWORD);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.id('logoutButton'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.id('logoutButton'));
        await browser.findElement(By.id('logoutButton')).click();

        await waitForElement(By.id('loginButton'));
    }

    async function checkAdmin() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[contains(., "Settings")]'));
        await browser.findElement(By.xpath('//a[contains(., "Settings")]')).click();

        await waitForElement(By.xpath('//a[contains(., "Save")]'));
        await browser.findElement(By.xpath('//a[contains(., "Save")]')).click();
    }

    async function createDevice(name) {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[contains(., "Add Client")]'));
        await browser.findElement(By.xpath('//a[contains(., "Add Client")]')).click();

        await waitForElement(By.id('newDeviceNameInput'));
        await browser.findElement(By.id('newDeviceNameInput')).sendKeys(name);
        await browser.sleep(3000);
        await browser.findElement(By.id('newDeviceSubmitButton')).click();
        await browser.sleep(5000);
    }

    async function checkDeviceExists(name) {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath(`//span[contains(., "${name}")]`)), TEST_TIMEOUT);
    }

    async function checkOpenVpn() {
        return new Promise((resolve, reject) => {
            const client = net.connect({host: app.fqdn, port: VPN_TCP_PORT}, () => {
                client.end();
                resolve();
            });

            client.on('error', reject);
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION} -p VPN_TCP_PORT=${VPN_TCP_PORT},WIREGUARD_UDP_PORT=${WIREGUARD_UDP_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, false));
    it('user is admin', checkAdmin);

    it('can create device', async () => createDevice(DEVICE_NAME));
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('check openvpn', checkOpenVpn);
    it('can logout', logout);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync('cloudron restart --app ' + app.id, EXEC_ARGS);
    });

    it('can login', login);
    it('user is admin', checkAdmin);
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('check openvpn', checkOpenVpn);
    it('can logout', logout);

    it('backup app', async function () {
        await browser.get('about:blank');
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION} -p VPN_TCP_PORT=${VPN_TCP_PORT},WIREGUARD_UDP_PORT=${WIREGUARD_UDP_PORT}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('user is admin', checkAdmin);
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('check openvpn', checkOpenVpn);
    it('can logout', logout);

    it('move to different location', function () { execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id} -p VPN_TCP_PORT=${VPN_TCP_PORT},WIREGUARD_UDP_PORT=${WIREGUARD_UDP_PORT}`, EXEC_ARGS); });
    it('can get new app information', getAppInfo);

    it('can login', login);
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('check openvpn', checkOpenVpn);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('can install app for update', function () { execSync(`cloudron install --appstore-id io.cloudron.openvpn --location ${LOCATION} -p VPN_TCP_PORT=${VPN_TCP_PORT},WIREGUARD_UDP_PORT=${WIREGUARD_UDP_PORT}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('user is admin', checkAdmin);
    it('can create device', async () => createDevice(DEVICE_NAME));
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('can logout', logout);
    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can login', login);
    it('user is admin', checkAdmin);
    it('device exists', async () => checkDeviceExists(DEVICE_NAME));
    it('check openvpn', checkOpenVpn);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
