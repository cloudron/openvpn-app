#!/bin/bash

set -eu

mkdir -p /run/vpn

export APP_ORIGIN=${CLOUDRON_APP_ORIGIN}
export APP_DOMAIN=${CLOUDRON_APP_DOMAIN}
export DATA_DIR=/app/data
export RUN_DIR=/run/vpn
export SERVER_NAME=cloudron

echo "==> Fixing permissions"
chown -R vpn:vpn /app/data /run/vpn

echo "Starting VPN"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenVPN
