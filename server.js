#!/usr/bin/env node

'use strict';

import compression from 'compression';
import crypto from 'crypto';
import cors from './src/cors.js';
import express from 'express';
import fs from 'fs';
import { HttpSuccess, HttpError } from 'connect-lastmile';
import lastMile from 'connect-lastmile';
import morgan from 'morgan';
import oidc from 'express-openid-connect';
import vpn from './src/vpn.js';
import safe from 'safetydance';
import child_process from 'child_process';

import { DATA_DIR, DNSMASQ_RUN_DIR, APP_ORIGIN } from './constants.js';
import database from './src/database.js';

function isAuthenticated (req, res, next) {
    if (!req.oidc.isAuthenticated()) return next(new HttpError(401, 'Unauthorized'));

    next();
}

async function isAdmin(req, res, next) {
    const [error, db] = await safe(database.read());
    if (error) return res.status(500).send({ message: 'cannot read db' });

    if (db.admins?.includes(req.oidc.user.sub)) return next();

    res.status(403).send({ message: 'Requires admin privileges' });
}

function isOpenVPN(req, res, next) {
    if (req.connection.remoteAddress !== '127.0.0.1') return next(new HttpError(401, 'Unauthorized'));

    next();
}

async function healthcheck (req, res) {
    const running = await vpn.isRunning();
    if (running) {
        res.status(200).send();
    } else {
        res.status(500).send();
    }
}

function oidcLogin(req, res) {
    res.oidc.login({
        returnTo: req.query.returnTo || '/',
        authorizationParams: {
            redirect_uri: `${APP_ORIGIN}/api/oidc/callback`,
        },
    });
}

async function getProfile(req, res, next) {
    const user = req.oidc.user;
    const username = req.oidc.user.sub;

    const [error, db] = await safe(database.read());
    if (error) return res.status(500).send({ message: 'cannot read db' });

    if (!db.admins) {
        console.log(`==> No admins in this instance. Making ${username} an admin`);
        db.admins = [ username ];
        await database.write(db);
    }

    user.isAdmin = db.admins?.includes(username);

    next(new HttpSuccess(200, { user }));
}

async function getSettings(req, res, next) {
    next(new HttpSuccess(200, { settings: await vpn.getSettings() }));
}

async function setSettings(req, res, next) {
    const [error] = await safe(vpn.setSettings(req.body.settings));
    if (error) return next(new HttpError(500, `Could not restart OpenVPN: ${error.message}`));
    next(new HttpSuccess(201, {}));
}

async function startServer() {
    const app = express();
    const router = new express.Router();

    const urlEncodedParser = express.urlencoded({extended: true});
    const jsonParser = express.json({strict: true});

    router.get ('/api/healthcheck', healthcheck);

    router.get ('/api/oidc/login',                                     oidcLogin);
    router.get ('/api/profile',  isAuthenticated,                      getProfile);
    router.get ('/api/settings', isAuthenticated, isAdmin,             getSettings);
    router.post('/api/settings', isAuthenticated, isAdmin, jsonParser, setSettings);
    router.get ('/api/list/',    isAuthenticated,                      vpn.list);
    router.put ('/api/key/*',    isAuthenticated,                      vpn.createKey);
    router.get ('/api/key/*',    isAuthenticated,                      vpn.getKey);
    router.delete('/api/key/*',  isAuthenticated,                      vpn.revokeKey);

    // hooks called by openvpn
    router.post('/api/onConnect/',      isOpenVPN, urlEncodedParser, vpn.onClientConnect);
    router.post('/api/onDisconnect/',   isOpenVPN, urlEncodedParser, vpn.onClientDisconnect);
    router.post('/api/onLearnAddress/', isOpenVPN, urlEncodedParser, vpn.onLearnAddress);

    app.set('trust proxy', 1);
    app.use(morgan('dev'));

    // currently for local development. vite runs on http://localhost:5555
    app.use(cors({ origins: [ '*' ], allowCredentials: true }));

    app.use(compression());
    app.use(oidc.auth({
        issuerBaseURL: process.env.CLOUDRON_OIDC_ISSUER,
        baseURL: APP_ORIGIN,
        clientID: process.env.CLOUDRON_OIDC_CLIENT_ID,
        clientSecret: process.env.CLOUDRON_OIDC_CLIENT_SECRET,
        secret: fs.readFileSync(`${DATA_DIR}/session.secret`, 'utf8'),
        session: {
            cookie: {
                sameSite: 'None',
            }
        },
        authorizationParams: {
            response_type: 'code',
            scope: 'openid profile email'
        },
        authRequired: false,
        routes: {
            callback: '/api/oidc/callback',
            login: false,
            logout: '/api/oidc/logout'
        }
    }));
    app.use(router);
    app.use('/', express.static('dist'));
    app.use(lastMile());

    const server = app.listen(3000, '0.0.0.0', () => {
        const {address, port} = server.address();

        console.log(`OpenVPN web-config interface listening at http://${address}:${port}`);
    });
}

async function generateSessionSecret() {
    if (!fs.existsSync(`${DATA_DIR}/session.secret`)) {
        console.log('==> Generating session secret');
        await fs.promises.writeFile(`${DATA_DIR}/session.secret`, crypto.randomBytes(256).toString('base64'));
    }
}

async function generateDnsmasqConfig() {
    if (!fs.existsSync(`${DATA_DIR}/dnsmasq.conf`)) {
        await fs.promises.writeFile(`${DATA_DIR}/dnsmasq.conf`, '# Add custom dnsmasq configuration here.\n# Be sure to restart the app after making changes.\n\n');
    }

    await fs.promises.mkdir(DNSMASQ_RUN_DIR, { recursive: true });

    child_process.execSync('supervisorctl start dnsmasq',  { encoding: 'utf8' });
}

(async function main() {
    console.log(`==> Starting VPN server with dataDir: ${DATA_DIR}`);

    await generateSessionSecret();
    await generateDnsmasqConfig();
    await vpn.init();
    await startServer();
})();
