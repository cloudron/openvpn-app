#!/bin/bash

set -eu

# create an OIDC client in your Cloudron with callback URL http://localhost:5555
export OIDC_ISSUER="${DASHBOARD_DEVELOPMENT_ORIGIN}"
export OIDC_CLIENT_ID="cid-b111cfaf86392ed5e01ed520d0cd0dd6"
export OIDC_CLIENT_SECRET="e9af976ea9f7f060b9184f187f6b7d92fd3ff7b52507abacc116d11acb90a6e7"

export VITE_API_ORIGIN=https://vpn.nebulon.space

npm run dev
